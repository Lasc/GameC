#include "hangman.h"
#include "stub.cpp"
#include <fstream>
#include <string>
#include <sstream>


int Hangman::readDictionary(void) {
    string word;
    int i = 0;
    int j;
    int k=0;

    ifstream dict("dictionary.txt");

    bool lengthExists;

    if (dict.is_open()) {
        while (getline(dict, word)) {


            lengthExists = false;
            for (j=0;j<=k;j++)
            {
                if (wordLengths[j] ==  word.length())
                {
                    lengthExists = true;
                }
            }
            // check if I already have this length in the array
            if (lengthExists == false)
            {
                wordLengths[k] = word.length();
                k++;
                wordLengths_size =k;
            }
            dict_list[i] = word;
            i++;
            dict_list_size = i;
        }
        dict.close();
       
    choose();

    return 0;

    }
    else
    {
        cout<<endl<<"Can not read the dictionary."<<endl;
        cout<<"Please check for following:"<<endl;
        cout<<" 1: Your folder contains the dictionary.txt file provided with the game."<<endl;
        cout<<" 2: Your dictionary file is empty."<<endl;
        cout<<" 3: Permissions to read the file are set accordingly on your system."<<endl;
        cout<<" "<<endl;
        return 1;
    }
}

Hangman::Hangman(void) {}


int Hangman::read_length(void) {
    int lengthIn;
    string in;
    int j=0;

    cout<< endl<<"The dictionary file contains words of lengths: "<<endl;
   
    for (j=0;j<wordLengths_size ; j++)
    {  
            cout << wordLengths[j]<<" ";
    }
    cout<<endl;

    cout << "Please enter length of the word you will be guessing: " << endl;
    while (true) {
        getline(cin, in);
        stringstream ss(in);

        if ((ss >> lengthIn) && !(ss >> in) && (lengthIn > 0)) {
	   		for (j=0;j<wordLengths_size ; j++)
	   		{  
		        if ( lengthIn == wordLengths[j])
				{
		        	length = lengthIn; 
		        	return length;
				}	
		    }
		}
        cin.clear();
        cerr
                << "Please enter length of the word, it must be a positive integer number in tha rage provided in the dictionary: "
                << endl;
    }
}

int Hangman::read_guesses(void) {
    int guessesNo;
    string in;

    cout << "Please enter number of guesses: " << endl;
    while (true) {
        getline(cin, in);
        stringstream ss(in);

        if (ss >> guessesNo && !(ss >> in) && (guessesNo > 0)) {
            guesses = guessesNo;
            return guessesNo;
        }
        cin.clear();
        cerr
                << "Please enter number of guesses, it must be a positive integer number: "
                << endl;
    }
}

