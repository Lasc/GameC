
#ifndef HANGMAN_H
#define HANGMAN_H
#include <iostream>
#include <fstream>
#include <string>
#include <cctype>
#include <cstring>

using namespace std;

class Hangman {

private:
    int guesses;  // number of guesses the user allows for the game
    int length;  // length of the word that the user will attempt to guess


    // operating list
    string op_list[100];
    int op_list_size;

    // dictionary list
    string dict_list[100];
    int dict_list_size;

    // chosen word by the computer
    string chosenWord;

    // contains the length of all the words in the dictionary
    // it is used to prompt the user to pick from the existing range
    int wordLengths[100];
    int wordLengths_size;

	// the word that the user forms with the correct letters it introducez
    string winnerCandidate;

public:
    Hangman();
    int readDictionary();
    int read_guesses();
    int read_length();
    void choose();
    void play(int);

};

#endif

