#include "hangman.cpp"



int main(int argc, char* argv[])
{
    Hangman h;
	
	int debugFlag = 0;

	string d ;
	if (argc==2)
	{	
		d.assign(argv[1]);

		if (d.compare("debug") == 0) 
			debugFlag = 1;
			
		else
			debugFlag = 0;
	}


    // read dictionary
    if (h.readDictionary() == 0)
    {

        //prompt for length & validate
        h.read_length();

        //prompt for guesses & validate
        h.read_guesses();

         // call play function
        h.play(debugFlag);
    }

    return 0;
}
