#include "hangman.h"
#include <cstdlib>

void Hangman::choose(void)
{
    int i;
    int j=0;

	// setting up the intial image of the operating list
    for (i=0;i<dict_list_size;i++)
    {
        if (dict_list[i].length() == length)
        {
            op_list[j] = dict_list[i];
            j++;
            op_list_size = j;
         }
    }  
   
    // choose a word for the fair version of the game
    // in dict_list chose randomly an index and
    // this will be the word
    srand(time(NULL));
    int random_index = (rand() % op_list_size) ;
    chosenWord.assign(op_list[random_index]);
}

void Hangman::play(int flag) {

    cout << endl;
    int i = 0;

    bool introduced = true;

    string introdString;
    char l;

    int remainingAttempts = guesses;
    choose ();
	

	if (flag == 1)
		cout<<"The chosen word is: "<<chosenWord<<endl;

    for (i = 0; i < length; i++) {
        winnerCandidate.push_back('-');
    }

    string letter;
    i = 0;
    bool win = false;

    while ((remainingAttempts>=1) && (win == false)) {
        cout << "Enter a letter:  ";
        cin >> letter;
        if (letter.length() == 1)
        {
            if (!isalpha(letter[0]))
            {
                cout << "Please enter a single letter: " << endl;
            }
            else
            {
                char l = letter[0];
                i++;
                // check is letter was introduced before
                int pos = introdString.find(l, 0);
                if (pos != -1)
                {
                    introduced = false;
                    cout<<"You've enetered this letter before. Please enter a different one!"<<endl;
                }
                else //check if the letter exists in the chosen word
                {
                    remainingAttempts--;
                    // search for letter in the chosen word 
                    int pos = chosenWord.find(l, 0);
                    if (pos != -1) {
                        int j;
                        for (j = 0; j <= chosenWord.length(); j++)
                        {
                            if (chosenWord[j] == l)
                                winnerCandidate.at(j) = l;
                        }
                        if (winnerCandidate.compare(chosenWord) == 0)
                        {
                            win = true;
                        }
                    }

                }
                //remember letters introduced so far
                introdString.push_back(l);

                cout << endl << endl << "All your letters so far: " << introdString << endl;
                cout << "Your word  so far: " << winnerCandidate << endl;
                cout << "You have "<<remainingAttempts<< " attempt(s) left"<<endl;
                cout <<endl;

            }
        }
        else
        {
            cout<< "Please enter only one letter:  ";
            cin>>letter;
        }
    }


    if (winnerCandidate.compare(chosenWord) != 0)
    {
        cout << "YOU LOST!" << endl;
        cout << "The winning word was: " << chosenWord << endl;
    }
    else
    {
        cout << "CONGRATULATIONS!!! You won!" << endl;
        cout << "The winning word was: " << chosenWord << endl;
        cout<<endl;
    }
}
